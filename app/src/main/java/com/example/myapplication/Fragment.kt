package com.example.myapplication

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputEditText
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class Fragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_, container, false)

        val button: Button = root.findViewById(R.id.button)
        button.setOnClickListener {


            this.findNavController().navigate(R.id.action_fragment3_to_camFragment)

        }

        button.setBackgroundColor(R.color.red as Int)

        val handler = Handler()

        val bar: ProgressBar = root.findViewById(R.id.progressBar)


        val textButton: Button = root.findViewById(R.id.textButton)
        textButton.setOnClickListener {

            bar.visibility = View.VISIBLE

            textButton.isActivated = false

            GlobalScope.launch {
                for (i in 0..100)
                {
                    handler.post(Runnable {
                        bar.progress = i
                    })
                    delay(10)
                }

                handler.post(Runnable {
                    bar.visibility = View.INVISIBLE
                    textButton.isEnabled = true

                    val text: TextView = root.findViewById(R.id.textView)
                    val input: TextInputEditText = root.findViewById(R.id.input)

                    text.text = input.editableText
                })
            }.start()

        }



        Toast.makeText(this.context, "Clicked", Toast.LENGTH_LONG).show()

        return  root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment Fragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            Fragment().apply {
            }
    }
}