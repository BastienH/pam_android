package com.example.myapplication

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText


/**
 * A simple [Fragment] subclass.
 * Use the [CamFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CamFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    lateinit var list: ArrayList<Int>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        setupPermissions()

        val root = inflater.inflate(R.layout.fragment_cam, container, false)

        val rv = root.findViewById<View>(R.id.recyclerView) as RecyclerView

        val cellList = ViewModelProvider(this).get(ModelData::class.java)
        
        // Create adapter passing in the sample user data
        val adapter = Adapter(cellList.getCells())
        // Attach the adapter to the recyclerview to populate items
        rv.adapter = adapter
        // Set layout manager to position the items
        rv.layoutManager = LinearLayoutManager(this.context)
        // That's all!

        val button: Button = root.findViewById(R.id.btn)
        button.setOnClickListener {
            cellList.addCell(cellList.getSize())
            rv.adapter?.notifyItemChanged(cellList.getSize())

        }

        return root
    }

    private val TAG = "Permission"
    private val RECORD_REQUEST_CODE = 101

    private fun setupPermissions() {
        val permission = this.context?.let {
            ContextCompat.checkSelfPermission(
                it,
                Manifest.permission.CAMERA)
        }

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to record denied")
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this.context as Activity,
            arrayOf(Manifest.permission.CAMERA),
            RECORD_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            RECORD_REQUEST_CODE -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                }
            }
        }
    }




    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment CamFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            CamFragment().apply {

            }
    }
}

