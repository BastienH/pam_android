package com.example.myapplication

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ModelData: ViewModel() {
    private val cells = MutableLiveData<ArrayList<Int>>()

    init {
        Log.e("MODEL", "Init")
        cells.value = ArrayList();
    }


    fun getCells(): LiveData<ArrayList<Int>> {
        return cells
    }

    fun addCell(c: Int) {
        cells.value?.add(c)
    }

    fun getSize() : Int
    {
        return cells.value?.size!!;
    }
}
