package com.example.myapplication

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        Log.d("DEBUG", "On Start")
    }

    override fun onResume() {
        super.onResume()
        Log.d("DEBUG", "On Resume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("DEBUG", "On Destroy")
    }

    override fun onStop() {
        super.onStop()
        Log.d("DEBUG", "On Stop")
    }

    override fun onPause() {
        super.onPause()
        Log.d("DEBUG", "On Pause")
    }

}